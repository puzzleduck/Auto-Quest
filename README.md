# AutoQuest

Automatically explore space.

[Live Deploy](https://puzzleduck.gitlab.io/Auto-Quest/)

## Development server

Run `ng serve --base-href Auto-Quest` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
Run `ng e2e --base-href Auto-Quest` to run Protractor tests in a headless chrome browser.

## CI

Simulate the CI environment wit by running:

```bash
sudo docker run -u $(id -u) --rm -v "$PWD":/app trion/ng-cli-e2e ng test --browser PhantomJS --single-run --progress=false
sudo docker run -u $(id -u) --rm -v "$PWD":/app trion/ng-cli-e2e ng e2e --base-href Auto-Quest
```
