import { AppPage } from './app.po';

describe('auto-quest App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    page.stopNg();
  });

  afterEach(() => {
    page.startNg();
  });

  it('should display game name', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toContain('Auto Quest');
  });
});
