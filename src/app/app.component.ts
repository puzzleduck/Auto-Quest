import { Component } from '@angular/core';
import { ShipComponent } from './ship/ship.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public Interval: any;
  title = 'Auto Quest (why seek?)';
  elapsedTick: number = 0;

  ngOnInit() {
    this.Interval = setInterval(() => {this.tick()}, 60);
  }

  tick() {
    this.elapsedTick += 1;
  }
}
