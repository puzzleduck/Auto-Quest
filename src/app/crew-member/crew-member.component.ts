import { Component, OnInit, Input } from '@angular/core';
import { CrewMember } from './crew-member'

@Component({
  selector: 'app-crew-member',
  templateUrl: './crew-member.component.html',
  styleUrls: ['./crew-member.component.css']
})
export class CrewMemberComponent implements OnInit {

  @Input() crewMember: CrewMember;

  constructor() { }


  ngOnInit() {
  }

}
