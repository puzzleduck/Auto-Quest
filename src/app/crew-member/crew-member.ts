export class CrewMember {
  name: string;
  experience: number;
  health: number;
}
