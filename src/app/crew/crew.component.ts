import { Component, OnInit } from '@angular/core';
import { CrewMember } from '../crew-member/crew-member'

@Component({
  selector: 'app-crew',
  templateUrl: './crew.component.html',
  styleUrls: ['./crew.component.css']
})
export class CrewComponent implements OnInit {

  selectedCrew: CrewMember;

  shipsCrew: [CrewMember] = [
    { name: "David Default", experience: 0, health: 99 },
    { name: "Daisy Default", experience: 0, health: 99 }
  ]

  constructor() { }

  ngOnInit() {
  }

  onSelect(crewMember) {
    this.selectedCrew = crewMember;
  }

}
