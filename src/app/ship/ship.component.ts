import { Component, OnInit } from '@angular/core';
import { Ship } from './ship'

@Component({
  selector: 'app-ship',
  templateUrl: './ship.component.html',
  styleUrls: ['./ship.component.css']
})
export class ShipComponent implements OnInit {
  public Interval: any;
  ship: Ship = {
    name: "So much for diplomacy",
    x_location: 0,
    y_location: 0,
    speed: 10
  };

  constructor() { }

  ngOnInit() {
    this.Interval = setInterval(() => {this.tick()}, 60);
  }

  tick() {
    let x_var = Math.random() - 0.5
    let y_var = Math.random() - 0.5
    this.ship.x_location += this.ship.speed * x_var;
    this.ship.y_location += this.ship.speed * y_var;
  }

}
