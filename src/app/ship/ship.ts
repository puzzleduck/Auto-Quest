export class Ship {
  name: string;
  x_location: number;
  y_location: number;
  speed: number;
}
